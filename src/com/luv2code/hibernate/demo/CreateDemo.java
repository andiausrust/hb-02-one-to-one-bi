package com.luv2code.hibernate.demo;

import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateDemo {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
										.configure("hibernate.cfg.xml")
										.addAnnotatedClass(Instructor.class)
										.addAnnotatedClass(InstructorDetail.class)
										.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			
			Instructor instructor = new Instructor("Andi2", "Mayer2", "familie2.mayer@aon.at");
			
			InstructorDetail detailInstructor = new InstructorDetail("my music", "luv2 code");
			
			//associate the objects
			instructor.setInstructorDetail(detailInstructor);
			
			session.beginTransaction();
			
			//save instructor -> will also save the detail object
			session.save(instructor);
			
			session.getTransaction().commit();
			
		} finally {
			// factory close
			factory.close();
		}
		
	}

}
 